###### 玩转金刚梯>金刚字典>
### 获得金刚号

#### 共有3种<strong> 获得方式 </strong>：

- 当您在[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)[ 注册 ](/LadderFree/kkDictionary/Register.md)后，[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)将自动派送您第1个[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)
  - 该号是[ 万能金刚号 ](/LadderFree/kkDictionary/KKIDMultipurpose.md)
  - 其上捆绑30GB [ 大宗流量 ](/LadderFree/kkDictionary/KKDataTrafficBulk.md)包
  - 该包是[ 免费流量 ](/LadderFree/kkDictionary/KKDataTrafficPackageFree.md)包
  - 该[ 流量包有效期 ](/LadderFree/kkDictionary/KKDataTrafficPackageExpiretion.md)1个月
- [ 连通 ](/LadderFree/kkDictionary/kkidsusage.md)[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)，登录[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)，在[ 金刚网>菜单>商店 ](https://www.atozitpro.net/zh/shop/)里购买[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)
- 给客服发邮件，客服将视具体情况决定是否派送您[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

