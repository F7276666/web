###### 玩转金刚梯>金刚字典>
### 获取副号，该如何操作？

- 连通[ 主号 ](/mainkkid.md)翻墙出来
- 登录永远被墙的[ 金刚网 ](/kksitecn.md)
- 在[ 金刚网>菜单>商店 ](https://atozitpro.net/zh/shop)里花0元购买一个捆绑每天0.2GB[ 免费流量 ](/kkdatatrafficfree.md)包的L2型[ 金刚号 ](/kkid.md)作为[ 副号 ](/auxiliarykkid.md)
- 配置时应[ 注意事项 ](/configurationconsiderations.md)

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

