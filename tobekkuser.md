###### 金刚梯>金刚帮助>金刚用户类>
### 成为金刚用户

使用[ 金刚VPN产品 ](/list_kkproducts.md)和[ 金刚VPN服务 ](/kkservices.md)即成为[ 金刚用户 ](/kkuser.md)

- 金刚梯获取渠道
  - 您可以扫他人散发的[ 邀请二维码1 ](/邀请二维码-广告商)或[ 邀请二维码2 ](/邀请二维码-推荐人)在[ 金刚网 ](/kksitecn.md)[ 注册 ](/reginkksitecn.md)以获取
  - 您也可以点击他人散发的[ 邀请链接1 ](/邀请链接-广告商) 或[ 邀请链接2 ](/邀请链接-推荐人)在[ 金刚网 ](/kksitecn.md)[ 注册 ](/reginkksitecn.md)以获取
  - 您还可以直接点击[ 翻墙神器金刚梯 ](/dlb.md)以获取

#### 推荐阅读
- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚1.0金刚号梯](/list_helpkkvpn1.0.md)
- [金刚2.0app梯](/list_helpkkvpn2.0.md)
- [金刚用户类](/list_kkuser.md)
